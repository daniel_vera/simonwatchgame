//
//  SimonGameIC.swift
//  SimonWatch
//
//  Created by Daniel Vera on 23/11/15.
//  Copyright © 2015 Daniel Vera. All rights reserved.
//

import WatchKit
import Foundation

protocol SimonGameDelegate {
    func didEndGameWithScore(score:Int)
}

class SimonGameIC: WKInterfaceController {

    var delegate: SimonGameDelegate?
    
    @IBOutlet var upperLeftButton: WKInterfaceButton!
    @IBOutlet var upperRightButton: WKInterfaceButton!
    @IBOutlet var lowerLeftButton: WKInterfaceButton!
    @IBOutlet var lowerRightButton: WKInterfaceButton!
    
    @IBOutlet var notificationLabel: WKInterfaceLabel!
    
    let gameTurnCount: Int = 1000
    var currentGameSequence: [Int]?
    var currentPlayerTurn: Int = 0
    var buttonsBlocked: Bool = false
    var userInputArray: NSMutableArray = NSMutableArray()
    
    //MARK: Life cycle
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        // Configure interface objects here.
        if (context != nil) {
            delegate = context as? SimonGameDelegate
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        startGame()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: Game logic
    func startGame()
    {
        currentPlayerTurn = 1
        buttonsBlocked = true
        generateNewGameSequence()
        
        notificationLabel.setText("Ready")
        var delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.notificationLabel.setText("Set")
            delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.notificationLabel.setText("Go !!")
                self.playSeriesForTurn(turnIndex: self.currentPlayerTurn)
            }
        }
    }
    
    func generateNewGameSequence()
    {
        currentGameSequence = [Int]()
        for (var i = 0;i < gameTurnCount; i++) {
            currentGameSequence?.append(Int(arc4random()%4))
        }
    }
    
    func playSeriesForTurn(turnIndex turnIndex:Int)
    {
        playSeriesFrom(index: 0, finishIndex: turnIndex)
    }
    
    func playSeriesFrom(index index:Int, finishIndex:Int)
    {
        if (index == finishIndex) {
            startPlayerTurn()
            return
        }
        
        let currentQuadrant: NSNumber = currentGameSequence![index]
        flashQuadrantWithIndex(currentQuadrant.longValue)
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.playSeriesFrom(index: index + 1, finishIndex: finishIndex)
        }
    }
    
    func startPlayerTurn()
    {
        notificationLabel.setText("Your Turn!")
        userInputArray = NSMutableArray()
        buttonsBlocked = false
    }
    
    func flashQuadrantWithIndex(quadrantIndex: Int)
    {
        let startingColor: UIColor = quadrantColors().objectAtIndex(quadrantIndex) as! UIColor
        let flashColor: UIColor = quadrantFlashColors().objectAtIndex(quadrantIndex) as! UIColor
        
        let buttonToFlash: WKInterfaceButton = gameButtons().objectAtIndex(quadrantIndex) as! WKInterfaceButton
        buttonToFlash.setBackgroundColor(flashColor)
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            WKInterfaceDevice.currentDevice().playHaptic(.Click)
            buttonToFlash.setBackgroundColor(startingColor)
        }
    }
    
    func startSimonTurn()
    {
        currentPlayerTurn++
        buttonsBlocked = true
        notificationLabel.setText("Nice Job. Next Turn")
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.playSeriesForTurn(turnIndex: self.currentPlayerTurn)
        }
    }
    
    func endGame()
    {
        notificationLabel.setText("Wrong!")
        WKInterfaceDevice.currentDevice().playHaptic(.Failure)
        
        if (delegate != nil) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.delegate?.didEndGameWithScore(self.currentPlayerTurn)
            }
        }
    }
    
    //MARK: Buttons events
    @IBAction func upperLeftAction() {
        playerPressedQuadrant(quadrant: 0)
    }
    
    @IBAction func upperRightAction() {
        playerPressedQuadrant(quadrant: 1)
    }
    
    @IBAction func lowerLeftAction() {
        playerPressedQuadrant(quadrant: 2)
    }
    
    @IBAction func lowerRightAction() {
        playerPressedQuadrant(quadrant: 3)
    }
    
    func playerPressedQuadrant(quadrant quadrant:Int)
    {
        if (buttonsBlocked) { return }
        
        userInputArray.addObject(quadrant)
        for (var i=0 ; i < userInputArray.count ; i++) {
            if ( userInputArray.objectAtIndex(i) as! Int != Int(currentGameSequence![i]) ) {
                endGame()
                return
            }
        }
        WKInterfaceDevice.currentDevice().playHaptic(.Success)
        
        if (userInputArray.count == currentPlayerTurn) {
            startSimonTurn()
        }
    }
    
    //MARK: Utility Methods
    func gameButtons() -> NSArray {
        return [self.upperLeftButton,self.upperRightButton,self.lowerLeftButton,self.lowerRightButton]
    }
    
    func quadrantColors() -> NSArray {
        return [UIColor.redColor(),UIColor.blueColor(),UIColor.greenColor(),UIColor.yellowColor()]
    }
    
    func quadrantFlashColors() -> NSArray
    {
        let flashColors: NSMutableArray? = NSMutableArray()
        let quadrantColorsArray: NSArray = quadrantColors()
        for (var i = 0;i < quadrantColorsArray.count; i++) {
            let flashColor: UIColor = quadrantColorsArray.objectAtIndex(i).colorWithAlphaComponent(0)
            flashColors?.addObject(flashColor)
        }
        return flashColors!
    }
}
