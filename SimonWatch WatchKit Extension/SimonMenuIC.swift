//
//  SimonMenuIC.swift
//  SimonWatch
//
//  Created by Daniel Vera on 23/11/15.
//  Copyright © 2015 Daniel Vera. All rights reserved.
//

import WatchKit
import Foundation


class SimonMenuIC: WKInterfaceController,SimonGameDelegate {

    @IBOutlet var gameOverLabel: WKInterfaceLabel!
    @IBOutlet var scoreLabel: WKInterfaceLabel!
    
    var playerScore: Int?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if (playerScore != nil) {
            gameOverLabel.setHidden(false)
            scoreLabel.setHidden(false)
            scoreLabel.setText("Score: \(playerScore!)")
            return
        }
        gameOverLabel.setHidden(true)
        scoreLabel.setHidden(true)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: SimonGameDelegate
    func didEndGameWithScore(score:Int)
    {
        playerScore = score
        self.popController()
    }
    
    //MARK: Navigation
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        if ( segueIdentifier == "GameICSegue") {
            return self
        }
        return nil
    }
}
